import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

class QuadTreeNode:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)  
        self.color = None
        self.children = None
        self.log_value = -2.0

class QuadTreeGrid:
    COLORS = 0
    EPSILON = 0.1
    DIRECTIONS = (
        np.array((1, 1)).reshape(1, 2),
        np.array((1, -1)).reshape(1, 2),
        np.array((-1, 1)).reshape(1, 2),
        np.array((-1, -1)).reshape(1, 2)
    )

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)   
        self.render_list = list()
        self.root = QuadTreeNode(
            parent=None,
            center=np.array([0, 0]),
            level=0,
            max_level=self.max_level,
            data_points = self.data_points,
            min_count=self.min_count,
            up_vals=self.up_vals,
            extent=self.grid_extent
        )
        self.fig, self.ax = plt.subplots(1)
        plt.xlim((-8.0, 8.0))
        plt.ylim((-8.0, 8.0)) 

    def update_quad_tree_grid(self, data_points):
        self._update_quad_tree(
            self.root, 
            data_points
        )

    def _update_quad_tree(self, node, data_points):
        pass

    def create_quad_tree_grid(self):
        self.data_angles = np.arctan2(
            self.data_points[0], 
            self.data_points[1]
        )
        self._split_quad_tree(self.root)
        return self.root

    def _split_quad_tree(self, node):
        level = node.level + 1
        if level >= self.max_level or node.data_points.size <= self.min_count:
            if node.data_points.size <= self.min_count:
                node.log_value = 2.0

            self.render_list.append(
                dict(
                    log_value=node.log_value,
                    center=node.center,
                    extent=node.extent
                )
            )

            return 

        node.children = list()
        for direction in self.DIRECTIONS:
            extent = node.extent / 2.0
            child_center = node.center + direction * extent
            child_data_points = QuadTreeGrid.pick_data_points(
                node.data_points, 
                child_center,
                extent
            )

            node.children.append(
                QuadTreeNode(
                    parent=node,
                    center=child_center,
                    level=node.level + 1,
                    data_points=child_data_points,
                    extent=extent
                )
            )

        for child in node.children:
            self._split_quad_tree(child)

    @staticmethod
    def pick_data_points(data_points, center, extent):
        picked_points = np.empty((0, 2))
        for point in data_points:
            box_condition = np.array([
                np.abs(center[0, 0] - point[0]),
                np.abs(center[0, 1] - point[1])
            ])

            if np.all(box_condition <= extent):
                picked_points = np.append(picked_points, point.reshape(1, 2), axis=0)

        return picked_points

    def get_color_code(self, log_value):
        if log_value > self.max_log_value:
            log_value = self.max_log_value
        if log_value < self.min_log_value:
            log_value = self.min_log_value

        grayscale = 255 - int(255 * (log_value + 2.0) / 4.0)
        return '#' + hex(grayscale)[2:].zfill(2) * 3

    def render_quad_tree_grid(self):
        for leaf_node in self.render_list:
            box = patches.Rectangle(
                (leaf_node['center'][0, 0] - leaf_node['extent'], leaf_node['center'][0, 1] - leaf_node['extent']),
                2 * leaf_node['extent'],
                2 * leaf_node['extent'],
                fill=True,
                facecolor=self.get_color_code(leaf_node['log_value']),
                linewidth=0.5,
                edgecolor='r'
            )
            self.ax.add_patch(box)
        
        plt.show()

    def segment_quad_tree_grid(self):
        self._color_occupied_nodes(self.root)

    def _color_occupied_nodes(self, node):
        if node.children is not None:
            for child in node.children:
                self._color_occupied_nodes(child)
            return

        if node.data_points.size == 0:
            return

        if node.color is None:
            node.color = self.COLORS
            self.COLORS += 1


