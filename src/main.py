import numpy as np
from quadtree import QuadTreeGrid


if __name__ == '__main__':
    samples = np.arange(0, np.pi / 3, 0.005)
    samples = np.append(samples, np.arange(np.pi / 2, 3 * np.pi / 4, 0.005))
    test_points = np.array((
        6 * np.cos(samples),
        6 * np.sin(samples)
    )).transpose()
    quad_tree = QuadTreeGrid(
        max_level=10,
        min_count=1,
        grid_extent=8,
        min_log_value=-2.0,
        max_log_value=2.0,
        data_points=test_points,
        up_vals=(1.5, -1.5)
    )
    quad_tree.create_quad_tree_grid()
    quad_tree.render_quad_tree_grid()